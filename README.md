# Requirements
* Manjaro KDE with latest updates installed
* executing user has passwordless sudo

### TODO
* ask for sudo pass
* install node
* install node packages for commitlint (@commitlint/cli @commitlint/config-conventional)
* install flameshot
* install unzip



### Shortcuts to edit
#### Remove shortcuts for
* CTRL+ALT+L
* ALT+F2
* CTRL+F8

#### Remapping shortcuts
* CTRL+ALT+T => Terminator
* PRINT => flameshot

#### Custom Rider shortcuts
* CTRL+W Close Tab
* CTRL+ALT+1 Open terminal
* CTRL+SHIFT+-/CTRL+SHIFT+NUMP- Collapse To Definition